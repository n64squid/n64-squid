<?php get_header();?>
<div class="grid_1">
	<div class="entry-content">
		<h1 class="entry-title">
			404 error
		</h1> 
		<p>The page has not been found.</p>
	</div>
</div>
<div class="grid_2 p5em">
<?php dynamic_sidebar('Single Post'); ?>
	<?php if ( is_active_sidebar( 'primary_widget_area_1' ) ) : 
		dynamic_sidebar( 'primary_widget_area_1' ); 
	endif; ?>
</div>
<!--    End dtls_pages -->
<?php get_footer();?>