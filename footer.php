       <div class="footer">
	   
            <div class="logo">
            
               <a href="<?php echo site_url(); ?>">
                <?php if(interior_get_logo()): ?>
                    <img  src="<?php echo interior_get_logo(); ?>">
                <?php else :?>
                    <img src="<?php echo get_stylesheet_directory_uri().'/images/logo.png'; ?>" />
                <?php endif;?>
             </a>
            
            </div>
               <div class="footer-container">
                  <?php 
                   if ( is_active_sidebar( 'secondary_widget_area_1' ) ) : 
                   dynamic_sidebar( 'secondary_widget_area_1' ); 
                   endif;
                  
                   if ( is_active_sidebar( 'secondary_widget_area_2' ) ) : 
                   dynamic_sidebar( 'secondary_widget_area_2' ); 
                   endif;                 
                
                   if ( is_active_sidebar( 'secondary_widget_area_3' ) ) :  
                   dynamic_sidebar( 'secondary_widget_area_3' ); 
                   endif;                 
                
                   if ( is_active_sidebar( 'secondary_widget_area_4' ) ) :  
                   dynamic_sidebar( 'secondary_widget_area_4' ); 
                   endif;
                   ?>                
               
            </div>
            <!--    End footer-container  --> 
            
            <!--<div class="footer-container-sidebar">
                <h5>Copyright &copy 2012 <a rel="nofollow" href="<?php echo site_url(); ?>" target="_blank"><?php echo get_option('blogname'); ?></a></h5>-->
                <!--<div class="socile-link">
                                 
                <?php $interior_options = get_option("interior_arc_admin");
                $interior_my_id =$interior_options['social']['facebook'];                        
                    if ($interior_my_id){
                ?>
                    <a href="<?php echo $interior_my_id ; ?>" title="Facebook"><img src="<?php echo get_template_directory_uri();?>/images/facebook.png" /> </a>  <?php } ?>
                
                <?php $interior_options = get_option("interior_arc_admin");
                $interior_my_id =$interior_options['social']['tweet'];                          
                    if ($interior_my_id){
                ?>
                    <a href="<?php echo $interior_my_id ; ?>" title="Twitter"><img src="<?php echo get_template_directory_uri();?>/images/twitter.png" /></a>  <?php } ?>
                    
                <?php $interior_options = get_option("interior_arc_admin");
                $interior_my_id =$interior_options['social']['google'];                          
                    if ($interior_my_id){
                ?>
                    <a href="<?php echo $interior_my_id ; ?>" title="Google"><img src="<?php echo get_template_directory_uri();?>/images/google.png" /></a>  <?php } ?>     
                </div>
                
                
            </div>--> 
            
    </div><!--    End footer-container  --> 
	</div><!--    End footer  --> 
	</div><!--    End full-div  -->
	<p class="footer-text">&#169; 2014-<?php echo date("y"); ?> N64 Squid</p>
    <?php wp_footer();?>
</body>
</html>