<div class="fanart">
  <?php query_posts("category_name=n64-fanart&showposts=12");   ?>
  <?php while(have_posts()): the_post(); ?>
        <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		  $default_attr = array(
);
  ?>
        <a rel="nofollow" style="background-image:url(<?php echo the_post_thumbnail_url('thumbnail'); ?>)" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" alt="<?php
  $myExcerpt = get_the_excerpt();
  $tags = array("<p>", "</p>");
  $myExcerpt = str_replace($tags, "", $myExcerpt);
  echo $myExcerpt;
  ?>" ></a>
        <?php
}  ?>
  <?php endwhile;?>
  <?php 
  echo do_shortcode('[ajax_load_more post_type="post" category="n64-fanart" offset="12" posts_per_page="24" scroll_distance="200" transition="none" transition_container="false"]');?>
</div>

