<?php
global $mxwidgets, $interior_layout_data, $interior_modules, $wp_widget_factory, $interior_modules_settings, $interior_options, $interior_layout_settings;
define("base_theme_url",get_template_directory_uri());

include("template-tags.php");
 
 

//Load interior active theme option data
function interior_load_theme_option_data(){
    global $interior_layout_data, $interior_modules, $interior_modules_settings, $interior_options;
    $interior_options = get_option("interior_arc_admin");     
    $interior_layout_data = get_option('interior_layout',array());    
    $interior_layout_settings = get_option('interior_layout_settings',array());        
    $interior_modules = get_option('interior_modules',array());        
    $interior_modules_settings = get_option('interior_modules_settings',array());        
    if($_GET['post']!=''){           
    $interior_layout_settings = get_post_meta($_GET['post'],'interior_layout_settings',true);                     
    $interior_layout_data = get_post_meta($_GET['post'],'interior_layout',true);
    $interior_modules = get_post_meta($_GET['post'],'interior_modules',true);        
    $interior_modules_settings = get_post_meta($_GET['post'],'interior_modules_settings',true);  
    
    }
    
}
  

//Raw JS Code
function interior_header_js(){
    ?>
    
    <script language="JavaScript">
    <!--
      var base_theme_url = "<?php echo base_theme_url; ?>",pageid="<?php echo $_GET['post']; ?>";
    //-->
    </script>
    
    <?php
}

//Save interior theme option
function interior_save_theme_options(){
   //  print_r($_POST['wpeden_admin']);
    //Save Layouts
    update_option("interior_arc_admin",$_POST['wpeden_admin']);
    update_option("interior_layout",$_POST['layouts']);
    update_option("interior_layout_settings",$_POST['layout_settings']);
    update_option("interior_modules",$_POST['modules']);
    update_option("interior_modules_settings",$_POST['modules_settings']);
    
    die();
}

//generate thumbnail based on selected admin option
function interior_thumb($post, $w, $h){
   global $interior_options; 
   $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large');   
   $imgurl = $thumb['0'];
   if($interior_options['general']['timthumb']==1)
   interior_timthumb($imgurl, $w, $h);
   else
   echo get_the_post_thumbnail($post->ID,array($w, $h),array('title'=>$post->post_title,'alt'=>$post->post_title));
   
}

function interior_timthumb($img, $w, $h){
    $img = urlencode($img);
    echo "<img src=\"".MX_THEME_URL."/timthumb.php?src=$img&w=$w&h=$h\" title='$t' alt='$a' class='$c' />"; 
}
 
//load fonts for the typography
function interior_get_fonts(){
    $ini_directory= MX_THEME_DIR.'/theme-data/';
    $font_array = parse_ini_file("$ini_directory/fonts.ini", true);
    return $font_array;    
}

//get the saved font for the typography
function interior_get_font($option){
    global $interior_options;
    $suffix=$option."_font";
    return  $interior_options['typography'][$suffix];
     //print_r($interior_options);
}

//get font family
function interior_font_family($option){
    global $interior_options;
    $suffix = $option."_font";
    $font =  $interior_options['typography'][$suffix];
    $fonts = interior_get_fonts();
    return $fonts[$font]['family'];
}

//get the saved font weight for the typography
function interior_get_font_weight($option){
    global $interior_options;
    $suffix=$option."_font_weight";
    return  $interior_options['typography'][$suffix];
     
}

//get the saved font color for the typography
function interior_get_font_color($option){
    global $interior_options;
    $suffix=$option."_font_color";
    return  $interior_options['typography'][$suffix];
     
}

//get the saved font size for the typography
function interior_get_font_size($option){
    global $interior_options;     
    $suffix=$option."_font_size";     
    return  $interior_options['typography'][$suffix];
     
} 

//get the saved font size type for the typography
function interior_get_font_size_type($option){
    global $interior_options;
    $suffix=$option."_font_size_type";
    return  $interior_options['typography'][$suffix];
     
}

//get font css
function interior_font_css($option){
    echo "
    font-family: ". interior_font_family($option) .";
    font-size: ". interior_get_font_size($option).interior_get_font_size_type($option)."; 
    font-weight: ". interior_get_font_weight($option)."; 
    color: ".interior_get_font_color($option).";
    ";
}

//enque google font
function interior_enqueue_google_fonts(){
    global $interior_options;    
    $fonts = interior_get_fonts();       
    $font =  $interior_options['typography']['general_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $font =  $interior_options['typography']['post_title_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $font =  $interior_options['typography']['post_meta_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $font =  $interior_options['typography']['post_content_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $font =  $interior_options['typography']['widget_title_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $font =  $interior_options['typography']['navigation_font'];
    if($fonts[$font]['type']=='google') $google_fonts[] = $font;
    $google_fonts = @implode("|",$google_fonts);
    wp_enqueue_style("interior-fonts","http://fonts.googleapis.com/css?family={$google_fonts}");
}

//enqueue admin styles
function interior_enqueue_admin_styles(){
    wp_enqueue_style("minicolor-css",base_theme_url.'/css/jquery.miniColors.css'); 
}

//script ans style for color picker
function interior_enqueue_admin_script(){
    wp_enqueue_script("jquery"); 
    wp_enqueue_script("minicolor-js",base_theme_url.'/js/jquery.miniColors.js');        
    wp_enqueue_script("color-js",base_theme_url.'/js/color-picker.js');     
} 

//return logo
function interior_get_logo(){
    global $interior_options;
    return $interior_options['general']['logo'];
} 

//return fav icon
function interior_get_favicon(){
    global $interior_options;
    return $interior_options['general']['favicon'];
}  

//return background texture
function interior_get_bgtexture(){
    global $interior_options;
    return $interior_options['general']['bg_texture'];
}

//return background color
function interior_get_bgcolor(){
    global $interior_options;
    return $interior_options['general']['bg_color'];
}

//return background image
function interior_get_bgimage(){
    global $interior_options;
   return $interior_options['general']['bg_texture'];
}

//return background style
function interior_get_bgstyle(){
   return $interior_options['general']['bg_style']; 
}

//Generate css
function interior_theme_styles(){
    global $interior_options;    
    ?>
    <style type="text/css">
   body{
     background: <?php echo interior_get_bgcolor(); ?> url("<?php echo interior_get_bgtexture(); ?>");
     <?php interior_font_css('general'); ?>  
   
   }
   
   .menu a{
     <?php interior_font_css('navigation'); ?>  
   }
   
   .entry-title a,
   .entry-title{
         <?php interior_font_css('post_title'); ?>
   }   
   
   .entry-content,
   .entry-content p{
         <?php interior_font_css('post_content'); ?>
   }   
   
   .widget h3,
   .widget-title{
         <?php interior_font_css('widget_title'); ?>
   }
   </style>
   <?php 
}

 



//Common Actions (required for front and back)
add_action('init','interior_load_theme_option_data');
 
if(is_admin()&&($_GET['page']=='interior-arc')){

    //Styles
    if($_GET['page']=='interior')
    wp_enqueue_style("admin-reset",base_theme_url.'/css/reset.css');
    wp_enqueue_style("admin-grid",base_theme_url.'/css/grid.css');
    wp_enqueue_style("admin-theme-style",base_theme_url.'/css/admin-style.css');     
    wp_enqueue_style("thickbox");
    
    //Scripts    
    wp_enqueue_script("jquery-ui-core");
    wp_enqueue_script("jquery-ui-sortable");
    wp_enqueue_script("jquery-form");
    wp_enqueue_script("thickbox");     
    wp_enqueue_script("interior-opr",base_theme_url.'/js/operations.js');
    
    //Actions    
    add_action('admin_head','interior_header_js');
    add_action("init","interior_enqueue_admin_script");
    add_action("init","interior_enqueue_admin_styles");
    

}

 

if(!is_admin()){
    
    add_action("wp_enqueue_scripts","interior_enqueue_google_fonts");
    //add_action("wp_head","interior_theme_styles");
 
}
 
    //Save all settings
    add_action("wp_ajax_interior_save_theme_options","interior_save_theme_options");
        