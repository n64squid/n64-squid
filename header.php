<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<?php $interior_options = get_option("interior_arc_admin");   
//add_action( 'wpseo_head', 'adjacent_rel_links');
remove_action( 'wpseo_head', 'adjacent_posts_rel_link_wp_head' );
wp_head();?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />  
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php
    /*
     * Print the <title> tag based on what is being viewed.
     */
    global $page, $paged; 
    wp_title( '|', true, 'right' ); 
    // Add the blog name.
    bloginfo( 'name' );  
    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";  
    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

    ?>
</title>
<link rel="icon" type="image/png" href="<?php echo interior_get_favicon(); ?>" />    


    
        <?php if($interior_options['general']['style_css']!=''){
           ?>
           
           <!--<link rel="Stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/<?php echo $interior_options['general']['style_css']; ?>.css" type="text/css" /> -->
           
           <?php 
            
        }
        ?> 
	<script type="text/javascript">
	$(function () {
		$("#slider").responsiveSlides({
			pager: false,
			speed: 500
		});
	});
	</script>
	<script type='text/javascript'>
		$(document).ready(function(){
			$('#primary_nav_wrap ul li:first-of-type').click(function(){
				$(this).siblings().slideToggle(200, function() {
				});
			})
		})
	</script>
</head>

<body itemscope itemtype="http://schema.org/WebPage"><div class="stones" style="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJWZT2H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="primary_nav_wrap">
		<?php wp_nav_menu(array('theme_location' => 'primary' , 'container' => '','menu_class' => 'sf-menu' )); ?>
	</div>
	
	</div>
    
			
				<?php if(is_home()){?>
             <div class="full-div">
			<div class="wrapper"> <?php //http://responsiveslides.com/ ?>
			 	<div class="container-a">
					<div class="dunno">
						<ul class="rslides" id="slider">
							<li>
								<a href="#" title="<?php the_title(); ?>">
									<img src="<?php echo get_stylesheet_directory_uri()."/images/banner.png"; ?>">
									<h2><span>Squidgy N64 articles, now in 3D!</span></h2>
								</a>
							</li>
							<?php  
							$my_id =$interior_options['slider']['cat'];                            
							$slide_no=$interior_options['slider']['count'];
							query_posts(
								array(
									'post_type' => array('post', 'homebrew', 'games'),
									'cat' => $my_id,
									'showposts' => $slide_no,
								)
							); 
							$i=0;
							while ( have_posts() ) : the_post();
							?>
							<li<?php echo $wp_corp_slide_action[$i];global $i; $i++; if ($i==4) $i=0;?>>
								<a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
									<?php the_post_thumbnail(); ?>
									<h2><span><?php the_title(); ?></span></h2>
								</a>
							</li>
							<?php   
							endwhile; 
							wp_reset_query();
							?>
						</ul>
					</div>   
				</div>
				<div class="container-a">
				<?php
				$interior_options = get_option("interior_arc_admin"); 
				$my_id = $interior_options['nwcollection']['cat'];
				query_posts("cat=$my_id&showposts=4");  
				while ( have_posts() ) : the_post();     ?>  
				<div class="container-pro">
					<h2 class="entry-title"><a class="entry-title" href="<?php the_permalink(); ?>" title="N64 <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<div class="container-pro-figer"><a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="N64 <?php the_title(); ?>" title="N64 <?php the_title(); ?>" /></a></div>
				</div>
					<!--    End container-pro   -->
				<?php endwhile; 
					wp_reset_query();
				?>
				</div>   
				<!--    End container-a   -->
				
<?php } ?>
