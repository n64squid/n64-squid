<?php 

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
 echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/css/font.css'.'">';
}

function interior_enqueue_scripts(){ 
 wp_enqueue_style( 'interior-style', get_stylesheet_uri(), array(), false, 'screen' );
 wp_enqueue_style( 'interior-1', get_stylesheet_directory_uri() . '/css/responsiveslides.css' );
 //wp_enqueue_style( 'interior-1', get_template_directory_uri() . '/css/style2.css' );
 
 wp_enqueue_script( 'jquery' );
 wp_enqueue_script( 'interior-12', 'https://code.jquery.com/jquery-2.2.0.min.js' );
 wp_enqueue_script( 'interior-11', get_stylesheet_directory_uri() . '/js/responsiveslides.min.js' );
 wp_enqueue_script( 'interior-13', 'https://raw.githubusercontent.com/viljamis/ResponsiveSlides.js/master/responsiveslides.min.js' );
 //wp_enqueue_script( 'interior-11', get_template_directory_uri() . '/js/jquery.flexslider-min.js' );
 
}
//remove_action( 'wpseo_head', '00000000050f653c000000001c3cb6cdadjacent_rel_links');
function wp_list_child_pages() { 

global $post; 

$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

if ( $childpages ) {

	$string = '<ul>' . $childpages . '</ul>';
}

return $string;

}

add_shortcode('wp_childpages', 'wp_list_child_pages');


function wp_list_category_posts($atts=array('cat'=>0,'long'=>false,'orderby'=>'title')) { 
	$postlist=get_posts(
			array(
				'post_type' => array('post', 'homebrew', 'games'),
				'category_name' => $atts['cat'],
				'posts_per_page' => -1,
				'orderby' => $atts['orderby'],
				'order' => 'asc'
			)
		); 
	if(!count($postlist)) {
		$postlist=get_posts(
			array(
				'post_type' => array('post', 'homebrew', 'games'),
				'tag' => $atts['cat'],
				'posts_per_page' => -1,
				'orderby' => $atts['orderby'],
				'order' => 'asc'
			)
		); 
	}
	if(count($postlist)){
		if ($atts['long']) {
			$out="<div class=\"wp_catposts\">";
			foreach ($postlist as $p){
				// Print the full post excerpt
				$out .= '<div class="wp_catposts_subdiv">'.
					'<h3><a href="'.get_permalink($p->ID).'">'.get_the_title($p->ID).'</a></h3>'.
					'<a href="'.get_permalink($p->ID).'">'.get_the_post_thumbnail($p->ID, 'medium').'</a>'.
					'<p>'.get_the_excerpt($p->ID).'</p>'.
				'</div>';
			}
			$out .= "</div>";
		} else {
			// Just print a plain list
			$out="<ul class=\"wp_catposts_list\">";
			foreach ($postlist as $p){
				$out.='<li><a href="'.get_permalink($p->ID).'">'.$p->post_title.'</a></li>';
			}
			$out.='</ul>';
		}
	} else {
		$out.='Nothing here yet.';
	}

	return $out;

}
add_shortcode('wp_catposts', 'wp_list_category_posts');



// Change the logo on the wp login page
function my_login_logo() { ?>
 <style type="text/css">
 #login h1 a, .login h1 a {
 background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
			width:100%;
			background-size: 100%;
			background-repeat: no-repeat;
			margin-bottom:0px;
 }
 </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Remove captcha popup
remove_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts' );

// Add 'Games' post type
function register_games() {
	register_post_type( 'games', array(
		'labels' => array(
			'name' => 'Games',
			'singular_name' => 'Game',
			'menu_name' => 'Games',
			'name_admin_bar' => 'Game',
			'add_new' => 'Add new',
			'add_new_item' => 'Add new game',
			'edit_item' => 'Edit game',
			'new_item' => 'New game',
			'view_item' => 'View game',
			'search_items' => 'Search games',
			'not_found' => 'No games found',
			'not_found_in_trash' => 'No games found in trash',
			'all_items' => 'All games'
		),
		'description' => 'Games to be listed on N64 Squid',
		'public' => true,
		'menu_position' => 20,
		'supports' => array('editor', 'title', 'custom-fields', 'thumbnail', 'excerpt', 'comments'),
		'show_in_rest' => true, // gutenberg
		'menu_icon' => 'dashicons-n64squid-n64-game-cart',
	));
}
add_action( 'init', 'register_games' );

// Add 'Homebrew' post type
function register_homebrew() {
	register_post_type( 'homebrew', array(
		'labels' => array(
			'name' => 'Homebrew',
			'singular_name' => 'Homebrew',
			'menu_name' => 'Homebrew',
			'name_admin_bar' => 'Homebrew',
			'add_new' => 'Add new',
			'add_new_item' => 'Add new homebrew',
			'edit_item' => 'Edit homebrew',
			'new_item' => 'New homebrew',
			'view_item' => 'View homebrew',
			'search_items' => 'Search homebrew',
			'not_found' => 'No homebrews found',
			'not_found_in_trash' => 'No homebrews found in trash',
			'all_items' => 'All homebrew'
		),
		'description' => 'Homebrew projects to be listed on N64 Squid',
		'public' => true,
		'menu_position' => 21,
		'supports' => array('editor', 'title', 'custom-fields', 'thumbnail', 'excerpt', 'comments'),
		'show_in_rest' => true, // gutenberg
		'taxonomies' => array( 'category', 'post_tag' ),
		'menu_icon' => 'dashicons-n64squid-n64-game-control',
		'rewrite' => array(
			'slug' => 'homebrew/roms',
		),
	));
}
add_action( 'init', 'register_homebrew' );
// Shortcode for the post list
function game_get_posts($atts) { 
	// Set vars
	$title = get_the_title();
	$fanartid = get_term_by('slug','n64-fanart', 'category')->term_id;
	$hacksid = get_term_by('slug','hb-hacks', 'category')->term_id;
	$out = "";

	// Set the array of category data
	$index = (object)array(
		'posts' => (object)array(
			'name' => "$title posts",
			'slug' => 'n64-fanart',
			'id' => NULL,
			'category__in' => NULL,
			'category__not_in' => array($fanartid,$hacksid)
		),
		'fanart' => (object)array(
			'name' => 'Fanart',
			'slug' => 'n64-fanart',
			'category__in' => $fanartid,
			'category__not_in' => NULL
		),
		'hacks' => (object)array(
			'name' => 'Hacks',
			'slug' => 'hb-hacks',
			'category__in' => $hacksid,
			'category__not_in' => NULL
		),
	);
	
	// Loop through categories
	foreach ($index as $i) {
		// Set query arguments
		$args = array(
			'posts_per_page' => -1,
			'orderby'=> 'date',
			'tag__in' => get_field('tag'),
			'category__not_in' => $i->category__not_in,
			'category__in' => $i->category__in
		);
		// Get posts, if any
		$postlist=get_posts($args);
		$remainder= count($postlist)%4;
		//Loop through posts
		if(count($postlist)){
			$out.="<h2>$i->name</h2>";
			$out.='<div class="games">';
			foreach ($postlist as $p){
				$out.='<a href="'.get_permalink($p->ID)."\" alt=\"$p->post_title\" title=\"$p->post_title: $p->post_excerpt\" style=\"background-image:url(".get_the_post_thumbnail_url($p->ID,'thumbnail').')">'.'</a>';
				//$out.=json_encode($p);
			}
			while ($remainder--) {
				$out.="<div class=\"blank-game\"></div>";
			}
			$out.='</div>';
		} else {
			// Output nothing
		}
	}

	// Print output
	return $out;

}
add_shortcode('wp_game_posts', 'game_get_posts');


// Shortcode for the post list
function get_all_games($atts) { 
	// Set vars
	$out = "";
	$args = array(
		'posts_per_page' => -1,
		'orderby'=> 'title',
		'post_type' => 'games'
	);
	// Get posts, if any
	$postlist=get_posts($args);
	//Loop through posts
	if(count($postlist)){
		$out.="<h2>$i->name</h2>";
		$out.='<div class="games">';
		foreach ($postlist as $p){
			$out.='<a href="'.get_permalink($p->ID)."\" alt=\"$p->post_title\" title=\"$p->post_name: $p->post_excerpt\" style=\"background-image:url(".get_the_post_thumbnail_url($p->ID,'thumbnail').')">'.'</a>';
			//$out.=json_encode($p);
		}
		$out.='</div>';
	} else {
		// Output nothing
		$out.='loldongs';
	}
	

	// Print output
	return $out;

}
add_shortcode('get_all_games', 'get_all_games');

// Sidebar widget
// Creating the widget 
class featuredWidget extends WP_Widget {
  
	function __construct() {
	parent::__construct(
	  
	// Base ID of your widget
	'featuredWidget', 
	  
	// Widget name will appear in UI
	__('Featured posts', 'featuredWidget_domain'), 
	  
	// Widget description
	array( 'description' => __( 'Sidebar thingy for putting certain categories in a feed', 'featuredWidget_domain' ), ) 
	);
	}
	  
	// Creating widget front-end
	  
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		  
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		  
		// This is where we get the list of posts to display
		$postlist=get_posts(
			array(
				'post_type' => array('post', 'homebrew', 'games'),
				'category_name' => $instance['category'] == "" ? "" : $instance['category'],
				'numberposts' => $instance['count'] == "" ? '1' : $instance['count'],
				'orderby' => $instance['sort'] == "" ? "orderby=date" : $instance['sort'],
				'order' => 'asc'
			)
		);

		$out = '<div class="featured-widget" id="featured-'.strtolower(str_replace(" ", "-", $title)).'">';
		// List the posts
		foreach ($postlist as $p){
			// Print the full post excerpt
			$out .= '<span title="'.$p->post_title.'"><a href="'.get_permalink($p->ID).'" target="_self">'.get_the_post_thumbnail($p->ID,'thumbnail', array( 'class' => 'wpp-thumbnail' )).'</a></span>
			';
		}
		$out .= '</div>';
		echo $out;

		echo $args['after_widget'];
	}
	          
	// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
	$title = __( 'New title', 'featuredWidget_domain' );
	}
	// Widget admin form
	?>
	<p>

		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />

		<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category slug:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>" />

		<label for="<?php echo $this->get_field_id( 'sort' ); ?>"><?php _e( 'Sort:' ); ?></label> <br>
		<select name="<?php echo $this->get_field_name( 'sort' ); ?>" id="<?php echo $this->get_field_id( 'sort' ); ?>">
			<option value="date">Date</option>
			<option value="rand">Random</option>
		</select><br>

		<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Count:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="number" value="<?php echo esc_attr( $count ); ?>" min="-1" />

	</p>
	<?php 
	}
	      
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';
		$instance['sort'] = ( ! empty( $new_instance['sort'] ) ) ? strip_tags( $new_instance['sort'] ) : '';
		$instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
		return $instance;
	}
	 
	// Class featuredWidget ends here
} 
 

// Register and load the widget
function featuredWidget() {
    register_widget( 'featuredWidget' );
}
add_action( 'widgets_init', 'featuredWidget' );


function theme_queue_js(){
if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') )
  wp_enqueue_script( 'comment-reply' );
}
add_action('wp_print_scripts', 'theme_queue_js');

function n64squid_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">         
            <div class="dtl-comment-box"> 
                <?php echo get_avatar($comment,$size='50'); ?>
                <h3> <?php printf(get_comment_author()) ?></h3>
                <div class="dtl-comment-box-replay"><?php comment_reply_link(array_merge( $args, array('reply_text' => __('Reply','Feather'),'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID
            , $comment->comment_post_ID) ?></div>      
                 <p><?php comment_text() ?></p>    
             </div>              
   </li>
<?php     
}
// Tell the recaptcha script not to run on pages that aren't the contact form
function wpcf7_dequeue_redundant_scripts() {
    $post = get_post();
    if ( is_singular() && !has_shortcode( $post->post_content, 'contact-form-7' ) ) {
        wp_dequeue_script( 'contact-form-7' );
        wp_dequeue_style( 'contact-form-7' );
        wp_dequeue_script( 'wpcf7-recaptcha' );     
        wp_dequeue_style( 'wpcf7-recaptcha' );
        wp_dequeue_script( 'google-recaptcha' );
    }
}
add_action( 'wp_enqueue_scripts', 'wpcf7_dequeue_redundant_scripts', 99 );
?>