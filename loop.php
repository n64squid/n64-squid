<?php
while ( have_posts() ) : the_post();     ?>
	<div class="grid_1_container_1st ">
		<a href="<?php the_permalink();?>">
			<?php the_post_thumbnail(); ?>
		</a>
		<h3>
			<a href="<?php the_permalink();?>">
				<?php the_title();?>
			</a>
		</h3>
		<p>
			<strong><?php the_date();?>:</strong> <?php the_excerpt(); ?>
		</p>
	</div>   
<?php endwhile; ?>