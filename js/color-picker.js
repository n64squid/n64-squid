jQuery(document).ready(function(){ 
    jQuery('#header_font_color').miniColors({
                
                change: function(hex, rgb) {
                    jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
                }
                
            }); 
    jQuery('#general_font_color').miniColors({
                
                change: function(hex, rgb) {
                    jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
                }
                
            }); 
    jQuery('#post_title_font_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    }); 
    jQuery('#post_meta_font_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    }); 
    jQuery('#post_content_font_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    }); 
    jQuery('#widget_title_font_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    });
    jQuery('#navigation_font_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    }); 
    
    jQuery('#bg_color').miniColors({
        
        change: function(hex, rgb) {
            jQuery("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
        }
        
    });
    
    
 }); 
   