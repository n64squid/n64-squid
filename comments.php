<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
	<?php
		return;
	}
?>                                    



    
<?php if ( have_comments() ) : ?>
 <div class="dtls_comment bar-div">
 
        <div class="bar entry-title"><h2 class="bar_h2_margin entry-title"><?php comments_popup_link('No COMMENT','1 COMMENT','% COMMENT','COMMENT'); ?> </h2></div>
         <ul >
                <?php
                   
                    wp_list_comments( array( 'callback' => 'n64squid_comment' ) );
                ?>
         </ul>
 </div>
     <div class="navigation">
            <div class="alignleft"><?php previous_comments_link() ?></div>
            <div class="alignright"><?php next_comments_link() ?></div>
        </div>
        <?php else : // this is displayed if there are no comments so far ?>

    <?php if ('open' == $post->comment_status) : ?>

     <?php else : // comments are closed ?>
        <!-- <p class="nocomments">Comments are closed.</p> -->

    <?php endif; ?>
<?php endif; ?>
 
    

 <?php if ('open' == $post->comment_status) : ?>

<div class="contact">

<div class="cancel-comment-reply">
    <small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
<?php else : ?>

<h2>Leave a Reply</h2>
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="contact" id="commentform">

<?php if ( $user_ID ) : ?>

<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

<?php else : ?>

                  <h6>Your Name (required)</h6> <input type="text" name="author" size="15" id="author" value="<?php echo $comment_author; ?>" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?>/><br />
                  <h6>Your Email (required)</h6> <input type="text" name="email" size="15" id="email" value="<?php echo $comment_author_email; ?>" <?php if ($req) echo "aria-required='true'"; ?> /><br />
                  <h6>Website</h6> <input type="text" name="url" size="15" value="<?php echo $comment_author_url; ?>" tabindex="3"/><br />
                  <?php endif; ?>
                  <h6>Your Message</h6> <textarea class="" id="comment" rows="10" cols="40" name="comment" tabindex="4"></textarea><br />
                  <div >
                    <p><input type="submit" name="submit" id="submit" tabindex="5" value="Submit Comment" style="height:40px; margin-top:40px; width:150px; margin-left:0px; color:#000" /></p>
                    <?php comment_id_fields(); ?> 
                  </div




><?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>
 </div>

<?php endif; // if you delete this the sky will fall on your head ?>


     
 

 
    