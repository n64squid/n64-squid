<?php get_header();?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
<div class="full-div">
<div class="grid_1">
  <div class="entry-content">
  	<article itemprop="mainEntity" itemscope itemtype="http://schema.org/Article">
        <h1 class="entry-title"  itemprop="mainEntityOfPage"><span itemprop="headline"><?php the_title();?></span></h1>
        <?php
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array(640,310) );
            if($url = $thumb['0']){ 
                the_post_thumbnail('post-thumbnail',array('itemprop' => 'image'));
            } ?>
        <p id="breadcrumbs">Posted <span itemprop="datePublished"><?php the_date('j M Y'); ?></span> by <span itemprop="author"><?php the_author(); ?></span><?php if (get_the_modified_time('U') >= get_the_time('U') + 1209600) {the_modified_date('j M Y', ', last modified <span itemprop="dateModified">','</span>');}?> </p>
        
        
        <div itemprop="articleBody">
          <?php the_content();?>
        </div>
        <h3>Articles across the web</h3>
        <div id="zergnet-widget-27795"></div>
        <script language="javascript" type="text/javascript">
			(function() {
				var zergnet = document.createElement('script');
				zergnet.type = 'text/javascript'; zergnet.async = true;
				zergnet.src = 'https://www.zergnet.com/zerg.js?id=27795';
				var znscr = document.getElementsByTagName('script')[0];
				znscr.parentNode.insertBefore(zergnet, znscr);
			})();
		</script>
        <div id="snippet-box">
				<div itemprop="description"><?php echo get_the_excerpt();?></div>
				<div itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">Article published on <span itemprop="name">N64 Squid</span><span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject"><img src="https://n64squid.com/logo.png" width="24" height="24" itemprop="url" class="schema-logo"/><meta itemprop="width" content="60"><meta itemprop="height" content="60"></span>
                <link href="<?php the_permalink(); ?>"></div>
		</div>
	</article>
    <?php 
			endwhile;
			endif;
		?>
  </div>
</div>
<div class="grid_2 p5em">
  <?php if(get_post_type() == 'games' || get_post_type() == 'homebrew' ) {include_once ('game-meta.php');} ?>
  <?php if ( is_active_sidebar( 'primary_widget_area_1' ) ) : 
		dynamic_sidebar( 'primary_widget_area_1' ); 
	endif; ?>
</div>
<?php 
	if (function_exists('wp_list_comments')) {
	comments_template('/comments.php', true);
	}
?>
</div>
<!--    End dtls_pages -->
<?php get_footer();?>
