<?php get_header();?>
<div class="grid_1">
	<div class="entry-content">
		<h1 class="entry-title">
			<?php single_tag_title(); ?>
		</h1> 
		<?php
		query_posts(
			array(
				'post_type' => array('post', 'homebrew', 'games'),
			)
		);
		include ("loop.php");
		interior_pagination();
		//wp_reset_query();
		?>
		
	</div>
</div>
<div class="grid_2 p5em">
<?php if ( function_exists('yoast_breadcrumb') )  {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
<?php dynamic_sidebar('Single Post'); ?>
	<?php if ( is_active_sidebar( 'primary_widget_area_1' ) ) : 
		dynamic_sidebar( 'primary_widget_area_1' ); 
	endif; ?>
</div>
<?php 
	if (function_exists('wp_list_comments')) {
	comments_template('/comments.php', true);
	}
?>
<!--    End dtls_pages -->
<?php get_footer();?>