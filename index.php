<?php get_header();?>

<div class="container-a" id="about-us">
	<div class="grid_1">
		<h2>Latest posts</h2>
		<?php
		$interior_options = get_option("interior_arc_admin"); 
		$my_id = $interior_options['portfolio']['cat'];
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		query_posts(
			array(
				'post_type' => array('post', 'homebrew', 'games'),
				'cat' => '-'.$my_id,
				'showposts' => 10,
				'paged' => $paged
			)
		);
		include ("loop.php");
		?>
		<div class="navigation">
			<p>
				<?php posts_nav_link(' - <a href="http://n64squid.com/all-posts/">All</a> - ','Newer ','Older'); ?>
			</p>
		</div>
	</div>
	<!--    End grid_1  -->
	
	<div class="grid_2">
		<?php 
		$interior_options = get_option("interior_arc_admin"); 
		$my_id = $interior_options['general']['home_page'];
		$interior_id = get_page($my_id); 
		$interior_title = $interior_id->post_title;          
		?>  
		<h2>
			<?php echo $interior_title;?>
		</h2>
		<p class="sidebar-para">
			<?php echo $interior_id->post_content ; ?> 
		</p>
		<?php if ( is_active_sidebar( 'primary_widget_area_1' ) ) : ?> 
		<?php dynamic_sidebar( 'primary_widget_area_1' ); ?>
		<?php endif; ?>
		
		<!--    End grid_2_container  -->
	</div> 
	<!--    End grid_2  -->		


</div>
<!--    End container-a  -->

<?php get_footer();?>