<?php

// Check id Advance Custom Fields Pro is installed
if(in_array( 'advanced-custom-fields-pro/acf.php', (array) get_option( 'active_plugins', array()))) {
	// Set the schema name for special types
	$schema = (object)array(
		'name' => 'itemprop="name"',
		'name_jp' => 'itemprop="alternativeHeadline"',
		'name_alt' => 'itemprop="alternativeHeadline"',
		'director' => 'itemprop="director"',
		'composer' => 'itemprop="musicBy" itemscope itemtype="http://schema.org/Person"><span itemprop="name"',
		'producer' => 'itemprop="producer"',
		'play_modes' => 'itemprop="playMode"',
		'players' => 'itemprop="numberOfPlayers"',
		'location' => 'itemprop="gameLocation"',
		'objective' => 'itemprop="quest"',
		'rom_size' => 'itemprop="fileSize"',
		'developer' => 'itemprop="author" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"',
		'publisher' => 'itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"',
		'awards' => 'itemprop="award"',
		'protagonist' => 'itemprop="character"',
		'average_score' => 'itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"',
		'release_date' => 'itemprop="datePublished"',
		'modified_date' => 'itemprop="dateModified"',
		'release_jp' => 'itemprop="datePublished"',
		'release_na' => 'itemprop="datePublished"',
		'release_pal' => 'itemprop="datePublished"',
		'made_in' => 'itemprop="locationCreated"',
		'game_length' => 'itemprop="timeRequired"',
		'age_range' => 'itemprop="typicalAgeRange"',
		'download' => '',
	);
	$schemaEnd = (object)array(
		'developer' => '</span>',
		'publisher' => '</span>',
		'composer' => '</span>',
		'rom_size' => 'MB',
		'download' => '">Download</a>',
	);
	echo "<dl class='game-meta' itemscope itemtype=\"http://schema.org/VideoGame\">";
	function cmp($a, $b) {
		return (int)$a['menu_order'] > (int)$b['menu_order'];
	}
	$aggregateScore = array();

	$customfields = get_field_objects();
	usort($customfields, "cmp");
	foreach ($customfields as $k => $v) {
		$v = (object)$v;
		$label = $v->name;
		$markup = isset($schema->$label)?" ".$schema->$label:'';
		$markupEnd = isset($schemaEnd->$label)?" ".$schemaEnd->$label:'';
		// Special cases
		if($v->name == 'price_id') {
			if(in_array( 'n64price/n64price.php', (array) get_option( 'active_plugins', array()))) {
				echo do_shortcode("[get_n64_price id=\"$v->value\"]");
			}
			continue;
		}
		if($v->name == 'game_length') {
			echo "<div class='game-meta-text'><dt>$v->label:</dt><dd>".substr($v->value, 1,65535)."</dd></div><meta itemprop=\"timeRequired\" content=\"$v->value\" >";
			continue;
		}
		// Switch through data type
		switch ($v->type) {
			case 'text':
			case 'date_picker':
			case 'button_group':
			case 'number':
			case 'select':
				if ($v->value) {
					echo "<div class='game-meta-text' title='$v->instructions'><dt>$v->label:</dt><dd$markup>$v->value$markupEnd</dd></div>";
				}
				break;
			case 'url':
				if ($v->value) {
					echo "<div class='game-meta-text' title='$v->instructions'><dt>$v->label:</dt><dd><a href='$v->value'>Download</a></dd></div>";
				}
				break;
			case 'checkbox':
				if ($v->value) {
					echo "<div class='game-meta-text'><dt>$v->label:</dt><dd>".implode(', ', $v->value)."</dd></div>";
				}
				break;
			case 'image':
				if ($v->value) {
					echo "<img src='".$v->value['url']."' />";
				}
				break;
			case 'gallery':
				if ($v->value) {
					echo "<div class='game-meta-gallery'>";
					$mode = 0;
					$i = 0;
					foreach ($v->value as $k =>$img) {
						if ($k && $mode==0) {
							// check rows to use
							$modulo = (count($v->value) - $k) % 3;
							if ($modulo == 0) {
								$mode = 3;
							} else if ($modulo == 2) {
								$mode = 2;
							} else {
								$mode = 1;
							}
							echo "<div class='game-meta-gallery-row-$mode'>";
						}
						echo "	<div class=\"game-meta-gallery-image-wrapper\">
									<a href=\"".$img['url']."\">
										<img src=\"".(($k == 0)?$img['sizes']['medium']:$img['sizes']['thumbnail'])."\"  itemprop=\"image\">
									</a>
								</div>";
						if($k && ++$i == $mode) {
							$mode = 0;
							$i = 0;
							echo "</div>";
						}
						
						
					}
					echo "</div>";
					
					break;
				}
			case 'range':
				if ($v->value) {
					if ($v->name == 'average_score') { // If we're doing the aggregate score only
						$v->value = round(array_sum($aggregateScore)/count($aggregateScore));
						$percent = 100 - $v->value;
						echo "<div class='game-meta-range' title='$v->instructions' style='background: #faa;background: linear-gradient(270deg, #fdd $percent%, #8f8 $percent%);'$markup><span class=\"score-text\">$v->label: <span itemprop=\"ratingValue\">$v->value</span>%</span><meta itemprop=\"bestRating\" content=\"100\" ><meta itemprop=\"worstRating\" content=\"0\" ><meta itemprop=\"ratingCount\" content=\"".count($aggregateScore)."\" ></div>";
					} else {
						array_push($aggregateScore, $v->value);
						$percent = 100 - $v->value;
						echo "<div class='game-meta-range' title='$v->instructions' style='background: #faa;background: linear-gradient(270deg, #fdd $percent%, #8f8 $percent%);'><span class=\"score-text\">$v->label: $v->value%</span></div>";
					}
					break;
				}
			case 'taxonomy':
				
				break;
			
			default:
				break;
		}
		
	}
	echo "<meta itemprop=\"applicationCategory\" content=\"Game\" ><meta itemprop=\"gamePlatform operatingSystem\" content=\"Nintendo 64\" ></dl>";
	//echo json_encode($customfields);
}
?>