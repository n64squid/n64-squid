<?php 
/*
Template Name: Full Width 
*/
get_header();?>
<div class="grid_1" style="width: 100%;">
	<div class="entry-content">
		<h1 class="entry-title">
			<?php the_title(); ?>
		</h1> 
		

<?php if(have_posts()) : while(have_posts()) : the_post();?>

		
		<?php
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array(640,310) );
		if($url = $thumb['0']){ 
			the_post_thumbnail();
		} ?>
		<?php if ( function_exists('yoast_breadcrumb') )  {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
		<p>
			<?php the_content(); ?>
		</p>
	<?php 
	endwhile;
	endif;
	if (get_the_title() == "Nintendo 64 Fanart"){ 
		include("loop-images.php");
	} else if (get_the_title() == "All posts"){
		query_posts("cat=-".get_cat_ID("Front box")."&showposts=30&paged=".$wp_query->query_vars['paged']);
		include("loop.php");
		?>
		<div class="navigation">
			<p>
				<?php posts_nav_link(' - <a href="http://n64squid.com/all-posts/">All</a> - ','Newer ','Older'); ?>
			</p>
		</div>
		<?php
	}?>
	</div>
</div>
<?php 
	if (function_exists('wp_list_comments')) {
	comments_template('/comments.php', true);
	}
?>
<!--    End dtls_pages -->
<?php get_footer();?>